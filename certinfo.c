#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <curl/curl.h>
#include <string.h>

#define BUFFER_MAX_LENGTH 1024
#define TEXT_CONVERSION_MAX_LEN 128

#define EXIT_FAILURE  1
#define EXIT_SUCCESS  0

int curl(char * line, char * folder_name);

/* <DESC>
 * Iterate URLs from the input list.
 * </DESC>
 */
int iter(int argc, char* argv[])
{
    FILE *file = NULL;
    char line[BUFFER_MAX_LENGTH];
    int tempChar = 1;
    unsigned int tempCharIdx = 0U;
    uint64_t url_num = 0;

    if (argc == 2)
        file = fopen(argv[1], "r");
    else {
        fprintf(stderr, "error: wrong number of arguments\n"
                        "usage: %s textfile\n", argv[0]);
        return EXIT_FAILURE;
    }

    if (!file) {
        fprintf(stderr, "error: could not open textfile: %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    /* get a character from the file pointer */
    while(tempChar)
    {
        tempChar = fgetc(file);
        /* avoid buffer overflow error */
        if (tempCharIdx == BUFFER_MAX_LENGTH) {
            fprintf(stderr, "error: line is too long. increase BUFFER_MAX_LENGTH.\n");
            return EXIT_FAILURE;
        }

        /* test character value */
        if (tempChar == EOF) {
            line[tempCharIdx] = '\0';
            fprintf(stdout, "%s\n", line);
            break;
        }
        else if (tempChar == '\n') {
            line[tempCharIdx] = '\0';
            tempCharIdx = 0U;
            fprintf(stdout, "%s\n", line);

            char command_mkdir[TEXT_CONVERSION_MAX_LEN]="mkdir ";
            char command_rmrf[TEXT_CONVERSION_MAX_LEN]="rm -rf ";

            char folder_name[TEXT_CONVERSION_MAX_LEN];
            sprintf(folder_name, "%llu", url_num);

            strcpy(command_mkdir + strlen(command_mkdir), folder_name);
            strcpy(command_rmrf + strlen(command_rmrf), folder_name);

            system(command_mkdir);

            if ( curl(line, folder_name) ) {
                system(command_rmrf);
            }
            url_num++;
            continue;
        }
        else
            line[tempCharIdx++] = (char)tempChar;
    }
    return EXIT_SUCCESS;
}

static size_t wrfu(void *ptr,  size_t  size,  size_t  nmemb,  void *stream)
{
    (void)stream;
    (void)ptr;
    return size * nmemb;
}


/* <DESC>
 * Extract lots of TLS certificate info.
 * </DESC>
 */
int curl(char * line, char * folder_name)
{
    CURL *curl;
    CURLcode res;

    uint8_t no_certinfo = 0;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, line);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wrfu);

        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
        curl_easy_setopt(curl, CURLOPT_CERTINFO, 1L);

        res = curl_easy_perform(curl);

        if(!res) {
            struct curl_certinfo *certinfo;

            res = curl_easy_getinfo(curl, CURLINFO_CERTINFO, &certinfo);

            if(!res && certinfo) {
                int i;

                //printf("%d certs!\n", certinfo->num_of_certs);
                FILE* file_chain_path_all = fopen("script_certs.txt", "a");
                fseek(file_chain_path_all, 0, SEEK_END);

                for(i = 0; i < certinfo->num_of_certs; i++) {
                    struct curl_slist *slist;
                    char path[TEXT_CONVERSION_MAX_LEN];
                    char path_url[TEXT_CONVERSION_MAX_LEN];
                    char filename[TEXT_CONVERSION_MAX_LEN]="cert";
                    char buffer[TEXT_CONVERSION_MAX_LEN];
                    sprintf(buffer,"%d",i);

                    strcpy(filename + strlen(filename),buffer);
                    strcpy(filename + strlen(filename) ,".cer");

                    strcpy(path, folder_name);
                    strcpy(path + strlen(path), "/");
                    strcpy(path_url, path);
                    strcpy(path + strlen(path), filename);
                    strcpy(path_url + strlen(path_url), "url.txt");




                    if ( !i ) {

                        fwrite("\"", 1, 1, file_chain_path_all);
                        fwrite(folder_name, 1, strlen(folder_name), file_chain_path_all);
                        fwrite("\" ", 1, 2, file_chain_path_all);
                        fwrite("verify_multiple auto advanced ", 1, strlen("verify_multiple auto advanced "),
                               file_chain_path_all);

                    }
                    fwrite("\"", 1, 1, file_chain_path_all);
                    fwrite(filename, 1, strlen(filename), file_chain_path_all);
                    fwrite("\" ", 1, 2, file_chain_path_all);
                    if (i == certinfo->num_of_certs - 1) {
                        fwrite("\n", 1, 1, file_chain_path_all);
                    }

                    FILE* file = fopen( path, "w");
                    if ( !i ) {
                        FILE *file_url = fopen(path_url, "w");
                        fwrite(line, 1, strlen(line), file_url);
                        fclose(file_url);
                    }

                    for(slist = certinfo->certinfo[i]; slist; slist = slist->next) {
                        printf("%s\n", slist->data);
                        fwrite(slist->data, 1, strlen(slist->data), file);
                    }
                    fclose(file);

                }
                if ( !i )  no_certinfo = 1;

                fclose(file_chain_path_all);
            }

        }
        curl_easy_cleanup(curl);
    }

    curl_global_cleanup();

    return (no_certinfo | res);
}

int main(int argc, char **argv)
{
    iter(argc, argv);
    return 0;
}
